package com.thehandsome.app.service;

import java.util.List;

import com.thehandsome.app.dto.ProductDTO;
import com.thehandsome.app.dto.ProductcolorDTO;

public interface ProductService {

	public List<ProductDTO> getProductlistByBrand(int brandid) throws Exception;

	public List<ProductcolorDTO> getProductColorById(String id) throws Exception;

	public List<ProductcolorDTO> getProductImageById(String id) throws Exception;
}
