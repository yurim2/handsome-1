package com.thehandsome.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.thehandsome.app.dao.ProductDAO;
import com.thehandsome.app.dto.ProductDTO;
import com.thehandsome.app.dto.ProductcolorDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	@Qualifier(value = "productDAO")
	private ProductDAO productDAO;

	@Override
	public List<ProductDTO> getProductlistByBrand(int brandid) throws Exception {
		return productDAO.selectProductByBrand(brandid);
	}

	@Override
	public List<ProductcolorDTO> getProductColorById(String id) throws Exception {
		return productDAO.selectProductcolorByID(id);
	}

	@Override
	public List<ProductcolorDTO> getProductImageById(String id) throws Exception {
		return productDAO.selectProductImageById(id);
	}

	


}
