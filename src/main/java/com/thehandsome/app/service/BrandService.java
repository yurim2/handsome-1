package com.thehandsome.app.service;

import java.util.List;

import com.thehandsome.app.dto.ProductDTO;

public interface BrandService {

	public int getBrandIdByName(String brandName) throws Exception;

}
