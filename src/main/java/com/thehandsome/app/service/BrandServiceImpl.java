package com.thehandsome.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.thehandsome.app.dao.BrandDAO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BrandServiceImpl implements BrandService {

	@Autowired
	@Qualifier(value = "brandDAO")
	private BrandDAO brandDAO;

	@Override
	public int getBrandIdByName(String brandName) throws Exception {
		return brandDAO.selectBrandIdByName(brandName);
	}


	


}
