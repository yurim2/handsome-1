package com.thehandsome.app.dto;

import lombok.Data;

@Data
public class ProductcolorDTO {
	private String productId;
	private String color;
	private String imagePath;
}
