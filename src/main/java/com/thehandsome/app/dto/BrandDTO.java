package com.thehandsome.app.dto;

import lombok.Data;

@Data
public class BrandDTO {
	int id;
	String name;
}
