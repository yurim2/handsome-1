package com.thehandsome.app.dto;

import lombok.Data;

@Data
public class DepartmentDTO {
	int id;
	String name;
	String address;
	double latitude;
	double longitude;
}
