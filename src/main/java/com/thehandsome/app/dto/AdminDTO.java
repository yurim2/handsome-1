package com.thehandsome.app.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class AdminDTO implements Serializable {
    private String id;
    private String password;
    private String branchId;
    

}