package com.thehandsome.app.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MemberDTO implements Serializable {
	private String id;
	private String password;
	private String name;
	private String gender;
	private String address;
	private String phone;
	private String birth;
	private int age;
	private int preferBrandId;
	private int preferDepartmentId;
	
}
