package com.thehandsome.app.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thehandsome.app.dto.AdminDTO;
import com.thehandsome.app.service.AdminService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(value="/admin")
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	
	@GetMapping(value="/signin")
	public String signinview(Model model) {

		return "admin/signin";
	}
	
	@PostMapping(value="/signinAction")
	public String signinAction(@ModelAttribute("adminDTO") AdminDTO adminDTO ,Model model, HttpSession session) {
		try {
			System.out.println("loginaction");
			System.out.println(adminDTO);
			AdminDTO admin = adminService.signin(adminDTO);
			System.out.println(admin);
			session.setAttribute("admin_id", admin.getId()); 

			
			return "redirect:/member/signup";
		} catch (Exception e) {
			
			e.printStackTrace();
			return "redirect:/admin/signin";
		} 
	}
}
