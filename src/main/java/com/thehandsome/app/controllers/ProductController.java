package com.thehandsome.app.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.thehandsome.app.dto.ProductDTO;
import com.thehandsome.app.dto.ProductcolorDTO;
import com.thehandsome.app.service.BrandService;
import com.thehandsome.app.service.ProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private BrandService brandService;

	
	@GetMapping("/brand/{brandName}")
	public String brandproduct(@PathVariable(required=false) String brandName, Model model) {
		try {
			int brandid = brandService.getBrandIdByName(brandName);
			List<ProductDTO> productlist = productService.getProductlistByBrand(brandid);
			model.addAttribute("productlist",productlist);
			
			Map<String, List<ProductcolorDTO>> productcolorlist = new HashMap<String, List<ProductcolorDTO>>();
			for(ProductDTO product : productlist) {
				List<ProductcolorDTO> eachproductcolorlist = productService.getProductColorById(product.getId());
				productcolorlist.put(product.getId(),eachproductcolorlist);
			}
			model.addAttribute("productcolorlist",productcolorlist);
			
			model.addAttribute("brandname",brandName);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "productlist/brand";
	}

	@GetMapping("/category/{gender}")
	public String genderproduct(@PathVariable String gender, Model model) {
//		try {
//			int brandid = brandService.getBrandIdByName(brandName);
//			List<ProductDTO> productlist = productService.getProductlistByBrand(brandid);
//			model.addAttribute("productlist",productlist);
//			
//			List<ProductcolorDTO> productcolorlist = new ArrayList<ProductcolorDTO>();
//			for(ProductDTO product : productlist) {
//				List<ProductcolorDTO> eachproductcolorlist = productService.getProductColorById(product.getId());
//				productcolorlist.addAll(eachproductcolorlist);
//			}
//			model.addAttribute("productcolorlist",productcolorlist);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		return "productlist/brand";
	}
	
	@GetMapping("/category/{gender}/{maincategoryId}")
	public String maincategoryproduct(@PathVariable(required=false) String gender, @PathVariable(required=false) int maincategoryId) {
		return "productlist/brand";
	}
	
	@GetMapping("category/{gender}/{maincategoryId}/{subcategoryId}")
	public String subcategoryproduct(@PathVariable(required=false) String gender, @PathVariable(required=false) int maincategoryId, @PathVariable(required=false) int subcategoryId) {
		return "productlist/brand";
	}
	
}
