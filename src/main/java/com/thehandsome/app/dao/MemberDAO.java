package com.thehandsome.app.dao;

import java.sql.SQLException;
import java.util.HashMap;

import com.thehandsome.app.dto.MemberDTO;



public interface MemberDAO {
	void signup(MemberDTO memberDTO) throws SQLException;

	MemberDTO selectMemberById(HashMap<String, Object> map) throws SQLException;

	MemberDTO selectMemberByPhone(HashMap<String, Object> map) throws SQLException;

	void insertMember(MemberDTO memberDTO) throws SQLException;

	MemberDTO selectMemberByIdPassword(MemberDTO memberDTO) throws SQLException;
}
