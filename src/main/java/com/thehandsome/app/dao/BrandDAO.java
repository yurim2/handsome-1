package com.thehandsome.app.dao;

import java.sql.SQLException;
import java.util.List;

import com.thehandsome.app.dto.BrandDTO;

public interface BrandDAO {


	public List<BrandDTO> selectAllBrand() throws SQLException;

	public int selectBrandIdByName(String brandName) throws SQLException;

}
