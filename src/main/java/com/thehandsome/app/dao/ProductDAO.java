package com.thehandsome.app.dao;

import java.sql.SQLException;
import java.util.List;

import com.thehandsome.app.dto.AdminDTO;
import com.thehandsome.app.dto.ProductDTO;
import com.thehandsome.app.dto.ProductcolorDTO;



public interface ProductDAO {

	List<ProductDTO> selectProductByBrand(int brandid) throws SQLException;

	List<ProductcolorDTO> selectProductcolorByID(String productid) throws SQLException;

	List<ProductcolorDTO> selectProductImageById(String productid) throws SQLException;

}
